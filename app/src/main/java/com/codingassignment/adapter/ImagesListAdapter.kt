package com.codingassignment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codingassignment.R
import com.codingassignment.constant.AppConstant
import com.codingassignment.databinding.HeaderItemRowBinding
import com.codingassignment.databinding.ImagesItemRowBinding
import com.codingassignment.model.ResponseData
import com.codingassignment.utils.HeaderItemDecoration
import com.codingassignment.utils.Utils


class ImagesListAdapter(
    private val context: Context, private val list: List<ResponseData.ImagesData>,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), HeaderItemDecoration.StickyHeaderInterface {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == AppConstant.VIEW_TYPE_HEADER) {
            val binding =
                HeaderItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolderHeader(binding)
        } else if (viewType == AppConstant.VIEW_TYPE_NORMAL) {
            val binding =
                ImagesItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolderItem(binding)
        }
        throw RuntimeException("There is no type that matches the type $viewType")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        with(list[position]) {
            when (holder) {
                is ViewHolderHeader -> {
                    with(holder) {
                        binding.tvHeader.text = headerTitle
                    }
                }
                is ViewHolderItem -> {
                    with(holder) {
                        Utils.loadImage(context, binding.img1, item1)
                        Utils.loadImage(context, binding.img2, item2)
                        Utils.loadImage(context, binding.img3, item3)
                        Utils.loadImage(context, binding.img4, item4)
                        Utils.loadImage(context, binding.img5, item5)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].viewType
    }

    inner class ViewHolderHeader(val binding: HeaderItemRowBinding) :
        RecyclerView.ViewHolder(binding.root)


    inner class ViewHolderItem(val binding: ImagesItemRowBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun getHeaderPositionForItem(itemPosition: Int): Int =
        (itemPosition downTo 0)
            .map { Pair(isHeader(it), it) }
            .firstOrNull { it.first }?.second ?: RecyclerView.NO_POSITION

    override fun getHeaderLayout(headerPosition: Int): Int {
        return R.layout.header_item_row
    }

    override fun bindHeaderData(header: View, headerPosition: Int) {
            val view =header as  TextView?
            if(view!=null){
                view.text=list[headerPosition].headerTitle
            }
        }

    override fun isHeader(itemPosition: Int): Boolean =
        list[itemPosition].viewType == AppConstant.VIEW_TYPE_HEADER

}