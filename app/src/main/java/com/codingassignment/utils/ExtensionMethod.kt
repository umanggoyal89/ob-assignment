package com.codingassignment.utils

import android.content.Context
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast

fun Context.showToast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
}

fun ProgressBar.showProgressBar(){
    visibility= View.VISIBLE
}


fun ProgressBar.hideProgressBar(){
    visibility= View.GONE
}