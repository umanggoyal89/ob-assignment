package com.codingassignment.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.Glide
import com.codingassignment.R


@SuppressLint("StaticFieldLeak")
object Utils {
    private var progressDialog: ProgressDialog? = null
    private fun showProgressDialog(context: Context): Dialog? {
        try {
            if (progressDialog != null && progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            progressDialog = ProgressDialog(context, R.style.NewDialog)
            progressDialog!!.setCancelable(false)
            progressDialog!!.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(context.resources, R.drawable.api_loader, null))
            progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            progressDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return progressDialog
    }

    fun dismissProDialog() {
        if (progressDialog != null) {
            progressDialog!!.dismiss()
        }
        progressDialog = null
    }

    private fun isDeviceOnline(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            try {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    return true
                }
            } catch (e: java.lang.Exception) {
                Log.i(context.getString(R.string.error), "" + e.message)
            }
        }
        return false
    }

    fun isNetworkAvailable(context: Context): Boolean {
        return if (isDeviceOnline(context)) {
            showProgressDialog(context)
            true
        } else {
            context.showToast(context.getString(R.string.no_internet))
            false
        }
    }

    fun loadImage(context: Context, view: ImageView, image: String?) {
        Glide.with(context).load(image)
            .centerCrop()
            .into(view)
    }
}