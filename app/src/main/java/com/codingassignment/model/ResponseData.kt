package com.codingassignment.model

data class ResponseData(val message: String, val status: String,
                        val data: List<ImagesData>) {
    data class ImagesData(
        val item1: String?="", val item2: String?="", val item3: String?="",
        val item4: String?="", val item5: String?="",var viewType:Int=0,
        var headerTitle:String="")
}