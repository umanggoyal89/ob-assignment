package com.codingassignment.constant

object AppConstant {
    const val VIEW_TYPE_NORMAL = 0
    const val VIEW_TYPE_HEADER = 1

    const val BASE_URL = "https://mocki.io/v1/"

    //EndPoint URLS
    const val IMAGES_LIST="d9ba4d6d-d609-4875-a0c7-3cb75ce88778"
}
