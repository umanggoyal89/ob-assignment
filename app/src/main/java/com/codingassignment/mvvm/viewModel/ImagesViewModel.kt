package com.codingassignment.mvvm.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.codingassignment.R
import com.codingassignment.api.ApiService
import com.codingassignment.model.ResponseData
import com.codingassignment.mvvm.repository.ImagesRepository
import com.codingassignment.utils.Utils
import com.codingassignment.utils.showToast
import kotlinx.coroutines.*

class ImagesViewModel(application: Application) :
    AndroidViewModel(application) {
    private var mApplication: Application = application
    private var imagesRepository: ImagesRepository = ImagesRepository(ApiService.invoke())

    val dataList = MutableLiveData<List<ResponseData.ImagesData>>()
    var job: Job? = null

    fun getResponseData() {
            job = CoroutineScope(Dispatchers.IO).launch {
                val response = imagesRepository.getImagesData()
                withContext(Dispatchers.Main) {
                    Utils.dismissProDialog()
                    if (response.isSuccessful && response.body() != null) {
                        if (response.body()!!.status == mApplication.getString(R.string.success)) {
                            with(response.body()!!) {
                                if (data.isNotEmpty())
                                    addHeaderInList(data)
                            }
                        } else {
                            onError(mApplication.getString(R.string.api_failure))
                        }
                    } else {
                        onError(mApplication.getString(R.string.api_failure))
                    }
                }
            }
    }

    private fun addHeaderInList(data: List<ResponseData.ImagesData>) {
        val list: MutableList<ResponseData.ImagesData> = mutableListOf()
        for (index in data.indices) {
            //for showing header
            val imagesData = ResponseData.ImagesData()
            imagesData.viewType = 1
            imagesData.headerTitle = "${mApplication.getString(R.string.header)} ${index + 1}"
            list.add(imagesData)

            list.add(data[index])
        }
        dataList.postValue(list)
    }

    private fun onError(message: String) {
        mApplication.showToast(message)
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}