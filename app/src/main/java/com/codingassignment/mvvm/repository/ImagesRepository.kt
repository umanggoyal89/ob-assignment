package com.codingassignment.mvvm.repository

import com.codingassignment.api.ApiService

class ImagesRepository(private val apiService: ApiService) {
     suspend fun getImagesData() = apiService.getImagesData()
}