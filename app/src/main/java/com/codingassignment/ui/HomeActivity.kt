package com.codingassignment.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.codingassignment.adapter.ImagesListAdapter
import com.codingassignment.databinding.ActivityHomeBinding
import com.codingassignment.model.ResponseData
import com.codingassignment.mvvm.viewModel.ImagesViewModel
import com.codingassignment.utils.HeaderItemDecoration
import com.codingassignment.utils.HeaderItemDecoration.StickyHeaderInterface
import com.codingassignment.utils.Utils


class HomeActivity : AppCompatActivity() {
    private lateinit var activityHomeBinding: ActivityHomeBinding
    private lateinit var imagesViewModel: ImagesViewModel
    private var imagesList: MutableList<ResponseData.ImagesData> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHomeBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(activityHomeBinding.root)

        val imagesListAdapter = ImagesListAdapter(this, imagesList)
        activityHomeBinding.recyclerViewList.adapter = imagesListAdapter
        activityHomeBinding.recyclerViewList.addItemDecoration(HeaderItemDecoration(
            activityHomeBinding.recyclerViewList,
            imagesListAdapter as StickyHeaderInterface))

        imagesViewModel = ViewModelProvider(this)[ImagesViewModel::class.java]
        imagesViewModel.dataList.observe(this) {
            if (it.isNotEmpty()) {
                imagesList.clear()
                imagesList.addAll(it)
                imagesListAdapter.notifyDataSetChanged()
            }
        }
        if (Utils.isNetworkAvailable(this)) {
            imagesViewModel.getResponseData()
        }
    }
}