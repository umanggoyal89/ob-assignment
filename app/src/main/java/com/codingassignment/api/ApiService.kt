package com.codingassignment.api

import com.codingassignment.constant.AppConstant
import com.codingassignment.model.ResponseData
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {

    @GET(AppConstant.IMAGES_LIST)
    suspend fun  getImagesData():Response<ResponseData>

    companion object{
       operator fun invoke():ApiService{
           return Retrofit.Builder()
               .addConverterFactory(GsonConverterFactory.create())
               .baseUrl(AppConstant.BASE_URL)
               .build()
               .create(ApiService::class.java)
       }
    }
}